import React from "react";
import Seo from "./Seo";

type Props = {};

const Wrapper = ({ children }: any) => {
  return (
    <div className="w-full">
      <Seo />
      <div className="max-w-[1366px] mx-auto ">{children}</div>
    </div>
  );
};

export default Wrapper;
