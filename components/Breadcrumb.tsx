import React from "react";

type Props = {};

const Breadcrumb = ({ title }: any) => {
  return (
    <div className="w-full bg-breadcrumb flex justify-center items-center bg-center bg-no-repeat bg-cover h-[100px] overflow-hidden rounded-br-xl rounded-bl-xl shadow-xl">
      <div className="w-full h-full flex flex-col justify-center items-center backdrop-brightness-50">
        <h1 className="text-white font-bold text-[30px]">{title}</h1>
      </div>
    </div>
  );
};

export default Breadcrumb;
