import React from "react";
import { AiFillFacebook, AiFillInstagram } from "react-icons/ai";

type Props = {};

const Footer = (props: Props) => {
  return (
    <footer className="w-full px-5 py-2 border-t-2 mt-5 flex justify-between items-center">
      <div className="flex gap-2">
        <p>
          <AiFillInstagram />
        </p>
        <p>
          <AiFillFacebook />
        </p>
      </div>
      <div>
        <p className="font-thin">2024 @ MIN 4 Serang - Hak cipta dilindungi</p>
      </div>
    </footer>
  );
};

export default Footer;
