import React, { useState } from "react";
import Image from "next/image";
import { Inter } from "next/font/google";
import logo2 from "@/public/logo/min4-logo2.png";
import logo1 from "@/public/logo/min4-logo1.png";
import Link from "next/link";

type Props = {};

const Header = (props: Props) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <header className="w-full px-6 py-3 flex justify-between items-center border-b-2">
      <div className="flex w-8/12 lg:w-5/12 gap-4 items-center ">
        <div className="w-5/12">
          <Image src={logo1} alt="logo" />
        </div>
        <div className="flex flex-col gap-1 text-left w-full ">
          <h3 className="font-bold lg:text-xl text-xs ">MIN 4 Serang</h3>
          <h3 className="font-light text-xs hidden lg:block ">
            Jl. Raya Pamarayan-Rangkas Bitung Kp. Leuwi Banteng Km.05 Ds.
            Sangiang Kec. Pamarayan Kabupaten Serang-Banten
          </h3>
          <h3 className="font-medium text-xs hidden lg:block">
            Email: min4serang09@gmail.com
          </h3>
        </div>
        <div className="lg:w-5/12 lg:flex hidden">
          <Image src={logo2} alt="logo" />
        </div>
      </div>

      <div className="w-6/12 lg:flex gap-5 cursor-pointer hidden">
        <div className="px-3 py-1 rounded-lg bg-white hover:bg-gray-100 transition-all ease-out duration-200">
          <Link href={`/`}>Home</Link>
        </div>
        <div className="px-3 py-1 rounded-lg bg-white hover:bg-gray-100 transition-all ease-out duration-200">
          <Link href={`/tentang-kami`}>Profil Madrasah</Link>
        </div>
        <div className="px-3 py-1 rounded-lg bg-white hover:bg-gray-100 transition-all ease-out duration-200">
          <Link href={`/arsip`}>Arsip Madrasah</Link>
        </div>
        <div className="px-3 py-1 rounded-lg bg-white hover:bg-gray-100 transition-all ease-out duration-200">
          <Link href={``}>Aplikasi</Link>
        </div>
        <div className="px-3 py-1 rounded-lg bg-white hover:bg-gray-100 transition-all ease-out duration-200">
          <Link href={`/hubungi-kami`}>Hubungi Kami</Link>
        </div>
      </div>

      <div className="lg:hidden flex flex-col relative">
        <button
          onClick={() => setIsMenuOpen(!isMenuOpen)}
          className="focus:outline-none"
        >
          <svg
            className="w-6 h-6"
            fill="none"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M4 6h16M4 12h16M4 18h16"
            ></path>
          </svg>
        </button>
        {isMenuOpen && (
          <div className="flex flex-col gap-2 mt-2 bg-white border rounded-lg shadow-lg absolute top-9 right-0 z-50 w-[200px]">
            <Link href={`/`}>
              <p className="px-3 py-2 rounded-lg hover:bg-gray-100 transition-all ease-out duration-200">
                Home
              </p>
            </Link>
            <Link href={`/tentang-kami`}>
              <p className="px-3 py-2 rounded-lg hover:bg-gray-100 transition-all ease-out duration-200">
                Profil Madrasah
              </p>
            </Link>
            <Link href={`/arsip`}>
              <p className="px-3 py-2 rounded-lg hover:bg-gray-100 transition-all ease-out duration-200">
                Arsip Madrasah
              </p>
            </Link>
            <Link href={``}>
              <p className="px-3 py-2 rounded-lg hover:bg-gray-100 transition-all ease-out duration-200">
                Aplikasi
              </p>
            </Link>
            <Link href={`/hubungi-kami`}>
              <p className="px-3 py-2 rounded-lg hover:bg-gray-100 transition-all ease-out duration-200">
                Hubungi Kami
              </p>
            </Link>
          </div>
        )}
      </div>
    </header>
  );
};
export default Header;
