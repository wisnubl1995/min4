import { gql } from "graphql-request";

export const SLIDER = gql`
  query sliders {
    sliders {
      id
      judul
      gambar {
        url
      }
    }
  }
`;

export const BERITA = gql`
  query beritas {
    beritas {
      id
      judulBerita
      detailBerita {
        html
        text
      }
      gambarBerita {
        url
      }
      slug
    }
  }
`;

export const KAMI = gql`
  query tentangKami {
    tentangKami(where: { id: "clu7xm8cwezot0719exfp3l1s" }) {
      visi
      misi {
        html
      }
    }
  }
`;
