/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["ap-southeast-2.graphassets.com"],
  },
};

export default nextConfig;
