import Breadcrumb from "@/components/Breadcrumb";
import Footer from "@/components/Footer";
import Header from "@/components/Header";
import Wrapper from "@/components/Wrapper";
import React from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Gallery } from "react-grid-gallery";
import { api } from "@/lib/api";
import { BERITA } from "@/lib/query";
// import Swiper core and required modules
import { Navigation, Pagination, Scrollbar, A11y } from "swiper/modules";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import Image from "next/image";
import Link from "next/link";

type Props = {};

export const getServerSideProps = async () => {
  const { beritas }: any = await api.request(BERITA);

  return {
    props: {
      beritas,
    },
  };
};

const Arsip = ({ beritas }: any) => {
  const images = [
    {
      src: "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_b.jpg",
      width: 320,
      height: 174,
      caption: "After Rain (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_b.jpg",
      width: 320,
      height: 212,
      tags: [
        { value: "Ocean", title: "Ocean" },
        { value: "People", title: "People" },
      ],
      alt: "Boats (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
      width: 320,
      height: 212,
    },
    {
      src: "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_b.jpg",
      width: 320,
      height: 174,
      caption: "After Rain (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_b.jpg",
      width: 320,
      height: 212,
      tags: [
        { value: "Ocean", title: "Ocean" },
        { value: "People", title: "People" },
      ],
      alt: "Boats (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
      width: 320,
      height: 212,
    },
    {
      src: "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_b.jpg",
      width: 320,
      height: 174,
      caption: "After Rain (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_b.jpg",
      width: 320,
      height: 212,
      tags: [
        { value: "Ocean", title: "Ocean" },
        { value: "People", title: "People" },
      ],
      alt: "Boats (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
      width: 320,
      height: 212,
    },
  ];
  return (
    <Wrapper>
      <Header />
      <Breadcrumb title={"Arsip"} />

      <div className="w-full py-10 px-2">
        <Tabs>
          <div className="cursor-pointer">
            <TabList
              className="flex flex-wrap gap-1"
              style={{
                justifyContent: "flex-start",
                marginBottom: "20px",
              }}
            >
              <Tab
                style={{
                  backgroundColor: "#4CAF50",
                  color: "#fff",
                  padding: "10px 20px",
                  borderRadius: "5px",
                  marginRight: "10px",
                }}
              >
                Kegiatan Siswa
              </Tab>
              <Tab
                style={{
                  backgroundColor: "#2196F3",
                  color: "#fff",
                  padding: "10px 20px",
                  borderRadius: "5px",
                  marginRight: "10px",
                }}
              >
                Berita
              </Tab>
              <Tab
                style={{
                  backgroundColor: "#FFC107",
                  color: "#fff",
                  padding: "10px 20px",
                  borderRadius: "5px",
                  marginRight: "10px",
                }}
              >
                Ekstrakulikuler
              </Tab>
              <Tab
                style={{
                  backgroundColor: "#FF5722",
                  color: "#fff",
                  padding: "10px 20px",
                  borderRadius: "5px",
                  marginRight: "10px",
                }}
              >
                Struktur Kurikulum
              </Tab>
            </TabList>
          </div>

          <div>
            <TabPanel>
              <div
                style={{
                  padding: "20px",
                  borderRadius: "5px",
                }}
              >
                <div className="mt-3">
                  <Gallery images={images} />
                </div>
              </div>
            </TabPanel>
            <TabPanel>
              <div
                style={{
                  padding: "20px",
                  borderRadius: "5px",
                }}
              >
                <div className="w-full py-3 lg:block hidden">
                  <Swiper
                    // install Swiper modules
                    modules={[Navigation, Pagination, Scrollbar, A11y]}
                    spaceBetween={10}
                    slidesPerView={4}
                    navigation
                    pagination={{ clickable: true }}
                  >
                    {beritas
                      .map((v: any, i: number) => {
                        const words = v.detailBerita.text.trim().split(/\s+/);
                        const excerpt = words.slice(0, 20).join(" ");
                        return (
                          <div key={v.id}>
                            <SwiperSlide>
                              <div className="w-full border-2 h-[300px] rounded-xl overflow-hidden p-2 flex flex-col">
                                <div className="w-full rounded-xl h-[300px] bg-slate-400 overflow-hidden">
                                  <Image
                                    src={v.gambarBerita.url}
                                    className="w-full"
                                    alt="berita"
                                    width={1000}
                                    height={1000}
                                  />
                                </div>
                                <div className="p-1 flex flex-col justify-between h-full">
                                  <div>
                                    <h3 className="font-black">
                                      {v.judulBerita}
                                    </h3>
                                    <p className="font-thin text-xs">
                                      {excerpt}
                                    </p>
                                  </div>
                                  <div>
                                    <div className="text-xs w-[5vw] flex justify-center items-center py-1 bg-green-400 rounded-xl cursor-pointer">
                                      <Link href={`/${v.slug}`}>Read More</Link>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </SwiperSlide>
                          </div>
                        );
                      })
                      .slice(0, 4)}
                  </Swiper>
                </div>

                <div className="w-full py-3 lg:hidden block">
                  <Swiper
                    // install Swiper modules
                    modules={[Navigation, Pagination, Scrollbar, A11y]}
                    spaceBetween={10}
                    slidesPerView={1}
                    navigation
                    pagination={{ clickable: true }}
                  >
                    {beritas
                      .map((v: any, i: number) => {
                        const words = v.detailBerita.text.trim().split(/\s+/);
                        const excerpt = words.slice(0, 20).join(" ");
                        return (
                          <div key={v.id}>
                            <SwiperSlide>
                              <div className="w-full border-2 h-[300px] rounded-xl overflow-hidden p-2 flex flex-col">
                                <div className="w-full rounded-xl h-[300px] bg-slate-400 overflow-hidden">
                                  <Image
                                    src={v.gambarBerita.url}
                                    className="w-full"
                                    alt="berita"
                                    width={1000}
                                    height={1000}
                                  />
                                </div>
                                <div className="p-1 flex flex-col justify-between h-full">
                                  <div>
                                    <h3 className="font-black">
                                      {v.judulBerita}
                                    </h3>
                                    <p className="font-thin text-xs">
                                      {excerpt}
                                    </p>
                                  </div>
                                  <div>
                                    <div className="text-xs lg:w-[5vw] flex justify-center items-center py-1 bg-green-400 rounded-xl cursor-pointer">
                                      <Link href={`/${v.slug}`}>Read More</Link>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </SwiperSlide>
                          </div>
                        );
                      })
                      .slice(0, 4)}
                  </Swiper>
                </div>
              </div>
            </TabPanel>
            <TabPanel>
              <div
                style={{
                  padding: "20px",
                  borderRadius: "5px",
                }}
              >
                <div className="mt-3">
                  <h3> Kegiatan Ekstrakurikuler TP 2023-2024</h3>
                  <ul className="list-disc">
                    <li>Futsal</li>
                    <li>Pencak Silat</li>
                    <li>Menari</li>
                    <li>Tahfid Quran</li>
                    <li>Bulutangkis</li>
                    <li>Hadroh/Qosidah</li>
                  </ul>
                </div>
              </div>
            </TabPanel>
          </div>
        </Tabs>
      </div>

      <Footer />
    </Wrapper>
  );
};

export default Arsip;
