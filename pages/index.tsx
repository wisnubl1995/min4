import Image from "next/image";
import { Inter } from "next/font/google";
import logo2 from "@/public/logo/min4-logo2.png";
import logo1 from "@/public/logo/min4-logo1.png";

import { Gallery } from "react-grid-gallery";

// import Swiper core and required modules
import { Navigation, Pagination, Scrollbar, A11y } from "swiper/modules";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import Header from "@/components/Header";
import Footer from "@/components/Footer";
import { api } from "@/lib/api";
import { BERITA, SLIDER } from "@/lib/query";
import Seo from "@/components/Seo";
import Link from "next/link";
import Wrapper from "@/components/Wrapper";

const inter = Inter({ subsets: ["latin"] });

export const getServerSideProps = async () => {
  const { sliders }: any = await api.request(SLIDER);
  const { beritas }: any = await api.request(BERITA);

  return {
    props: {
      sliders,
      beritas,
    },
  };
};

export default function Home({ sliders, beritas }: any) {
  console.log(beritas);
  const data = [
    {
      id: 1,
      name: "Slide 1",
    },
    {
      id: 2,
      name: "Slide 2",
    },
    {
      id: 3,
      name: "Slide 3",
    },
    {
      id: 4,
      name: "Slide 4",
    },
  ];

  const images = [
    {
      src: "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_b.jpg",
      width: 320,
      height: 174,
      caption: "After Rain (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_b.jpg",
      width: 320,
      height: 212,
      tags: [
        { value: "Ocean", title: "Ocean" },
        { value: "People", title: "People" },
      ],
      alt: "Boats (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
      width: 320,
      height: 212,
    },
    {
      src: "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_b.jpg",
      width: 320,
      height: 174,
      caption: "After Rain (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_b.jpg",
      width: 320,
      height: 212,
      tags: [
        { value: "Ocean", title: "Ocean" },
        { value: "People", title: "People" },
      ],
      alt: "Boats (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
      width: 320,
      height: 212,
    },
    {
      src: "https://c2.staticflickr.com/9/8817/28973449265_07e3aa5d2e_b.jpg",
      width: 320,
      height: 174,
      caption: "After Rain (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_b.jpg",
      width: 320,
      height: 212,
      tags: [
        { value: "Ocean", title: "Ocean" },
        { value: "People", title: "People" },
      ],
      alt: "Boats (Jeshu John - designerspics.com)",
    },
    {
      src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
      width: 320,
      height: 212,
    },
  ];

  return (
    <Wrapper>
      <Header />

      <main className="mt-5 rounded-xl overflow-hidden">
        <Swiper
          // install Swiper modules
          modules={[Navigation, Pagination, Scrollbar, A11y]}
          spaceBetween={50}
          slidesPerView={1}
          navigation
          pagination={{ clickable: true }}
          scrollbar={{ draggable: true }}
          onSwiper={(swiper) => console.log(swiper)}
          onSlideChange={() => console.log("slide change")}
          className="lg:h-[30vw] h-[100vw]"
        >
          {sliders.map((v: any, i: number) => {
            const image = v.gambar.url;

            return (
              <div key={v.id}>
                <SwiperSlide>
                  <div
                    style={{ backgroundImage: `url(${image})` }}
                    className={`flex flex-col justify-center bg-no-repeat bg-center bg-cover bg-blend-color-dodge items-center h-full`}
                  >
                    <div className="w-full h-full flex flex-col justify-center items-center backdrop-brightness-50">
                      <h3 className="font-bold lg:text-[60px] text-[20px] text-white">
                        {v.judul}
                      </h3>
                      <div className="lg:mt-24 mt-5 px-6 py-1 rounded-xl bg-green-400 cursor-pointer text-white">
                        Read More
                      </div>
                    </div>
                  </div>
                </SwiperSlide>
              </div>
            );
          })}
        </Swiper>
      </main>

      <div className="mt-10 w-full px-3">
        <div>
          <h3 className="text-xl font-semibold">Berita Terkini</h3>
        </div>
        {/* Desktop */}
        <div className="w-full py-3 hidden lg:block">
          <Swiper
            // install Swiper modules
            modules={[Navigation, Pagination, Scrollbar, A11y]}
            spaceBetween={10}
            slidesPerView={4}
            navigation
            pagination={{ clickable: true }}
          >
            {beritas
              .map((v: any, i: number) => {
                const words = v.detailBerita.text.trim().split(/\s+/);
                const excerpt = words.slice(0, 20).join(" ");
                return (
                  <div key={v.id}>
                    <SwiperSlide>
                      <div className="w-full border-2 h-[300px] rounded-xl overflow-hidden p-2 flex flex-col">
                        <div className="w-full rounded-xl h-[300px] bg-slate-400 overflow-hidden">
                          <Image
                            src={v.gambarBerita.url}
                            className="w-full"
                            alt="berita"
                            width={1000}
                            height={1000}
                          />
                        </div>
                        <div className="p-1 flex flex-col justify-between h-full">
                          <div>
                            <h3 className="font-black">{v.judulBerita}</h3>
                            <p className="font-thin text-xs">{excerpt}</p>
                          </div>
                          <div>
                            <div className="text-xs w-[5vw] flex justify-center items-center py-1 bg-green-400 rounded-xl cursor-pointer">
                              <Link href={`/${v.slug}`}>Read More</Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </SwiperSlide>
                  </div>
                );
              })
              .slice(0, 4)}
          </Swiper>
        </div>
        {/* Mobile */}
        <div className="w-full py-3 lg:hidden block">
          <Swiper
            // install Swiper modules
            modules={[Navigation, Pagination, Scrollbar, A11y]}
            spaceBetween={10}
            slidesPerView={1}
            navigation
            pagination={{ clickable: true }}
          >
            {beritas
              .map((v: any, i: number) => {
                const words = v.detailBerita.text.trim().split(/\s+/);
                const excerpt = words.slice(0, 20).join(" ");
                return (
                  <div key={v.id}>
                    <SwiperSlide>
                      <div className="w-full border-2 h-[300px] rounded-xl overflow-hidden p-2 flex flex-col">
                        <div className="w-full rounded-xl h-[300px] bg-slate-400 overflow-hidden">
                          <Image
                            src={v.gambarBerita.url}
                            className="w-full"
                            alt="berita"
                            width={1000}
                            height={1000}
                          />
                        </div>
                        <div className="p-1 flex flex-col justify-between h-full">
                          <div>
                            <h3 className="font-black">{v.judulBerita}</h3>
                            <p className="font-thin text-xs">{excerpt}</p>
                          </div>
                          <div>
                            <div className="text-xs lg:w-[5vw] flex justify-center items-center py-1 bg-green-400 rounded-xl cursor-pointer">
                              <Link href={`/#`}>Read More</Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </SwiperSlide>
                  </div>
                );
              })
              .slice(0, 4)}
          </Swiper>
        </div>
      </div>

      <div className="mt-10 w-full px-3">
        <div>
          <h3 className="text-xl font-semibold">Tentang Kami</h3>
        </div>

        <div className="w-full py-3 flex gap-2 lg:flex-row flex-col-reverse justify-between">
          <div className="lg:w-6/12 flex flex-col gap-2">
            <p className="text-sm">
              Puji dan syukur marilah kita panjatkan ke hadirat Illahi Rabbi,
              atas karunia-Nya kita senantiasa diberikan kesempatan dalam rangka
              thalabul ilmi, mencari ilmu. Mudah-mudaham setiap derap langkah
              bisa membuahkan pahala bagi kita semua, menjadi penghapus dosa dan
              pengangkat derajat di hadapan Allah Swt. Tak lupa semoga shalawat
              serta salam senantiasa tercurah kepada junjungan kita Nabi
              Muhammad Saw, kepada keluarganya, sahabatnya, yang menjadi suri
              tauladan bagi seluruh umatnya hingga akhir zaman.
            </p>

            <p className="text-sm">
              Madrasah merupakan lembaga yang membekali siswa dengan pendidikan
              formal dan pengetahuan agama untuk membentuk karakter siswa yang
              cerdas, beriman, dan bertaqwa. Seiring dengan perkembangan zaman,
              selain pendidikan islam, madrasah mulai dilengkapi penguasaan
              teknologi informasi dan ketrampilan modern. Untuk itu, madrasah
              memiliki peranan penting dalam masyarakat guna mendukung
              peningkatan kualitas sumber daya manusia.
            </p>
          </div>
          <div className="lg:w-6/12 flex justify-center items-center">
            <div>
              <Image src={logo1} alt="logo" />
            </div>
          </div>
        </div>
      </div>

      <div className="mt-10 w-full px-3">
        <div>
          <h3 className="text-xl font-semibold">Galeri Sekolah</h3>
        </div>

        <div className="mt-3">
          <Gallery images={images} />
        </div>
      </div>

      <Footer />
    </Wrapper>
  );
}
