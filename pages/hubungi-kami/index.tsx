import Breadcrumb from "@/components/Breadcrumb";
import Footer from "@/components/Footer";
import Header from "@/components/Header";
import Wrapper from "@/components/Wrapper";
import React, { useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

type Props = {};

const Index = (props: Props) => {
  const [loading, setLoading] = useState(false);
  const [recaptchaValue, setRecaptchaValue] = useState<string | null>(null);

  const contactHandler = async (event: any) => {
    event.preventDefault();
    if (!recaptchaValue) {
      toast.error("Re-captcha is Failed");
      return;
    }
    const target = event.target;
    const { email, firstName, lastName, phoneNumber, company, message } =
      target;
    try {
      setLoading(true);
      const response = await fetch("/api/email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email.value,
          firstName: firstName.value,
          lastName: lastName.value,
          phoneNumber: phoneNumber.value,
          company: company.value,
          message: message.value,
          recaptchaValue,
        }),
      });
      if (response.ok) {
        toast.success("Your message has been sent.", { autoClose: 3000 });
        setLoading(false);
      } else {
        toast.error("Failed to send your message. Please try again.");
        setLoading(false);
      }
    } catch (error) {
      toast.error("INTERNAL ERROR");
    }
  };

  return (
    <Wrapper>
      <Header />
      <Breadcrumb title={"Hubungi Kami"} />
      <div className="isolate bg-white px-6 py-10 lg:py-24 sm:py-32 lg:px-8 font-QuicksandRegular">
        <div className="container mx-auto p-8 mt-10 bg-white rounded shadow-lg grid grid-cols-1 md:grid-cols-2 gap-8">
          <div>
            <form onSubmit={contactHandler} className="mx-auto  max-w-xl ">
              <div className="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
                <div>
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    First name
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="text"
                      name="firstName"
                      id="firstName"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div>
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Last name
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="text"
                      name="lastName"
                      id="lastName"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Company
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="text"
                      name="company"
                      id="company"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Email
                  </label>
                  <div className="mt-2.5">
                    <input
                      type="email"
                      name="email"
                      id="email"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Phone number
                  </label>
                  <div className="relative mt-2.5">
                    <input
                      type="tel"
                      name="phoneNumber"
                      id="phoneNumber"
                      className="block w-full rounded-md border-0 px-3.5 py-2 pl-20 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    />
                  </div>
                </div>
                <div className="sm:col-span-2">
                  <label className="block text-sm font-semibold leading-6 text-gray-900">
                    Message
                  </label>
                  <div className="mt-2.5">
                    <textarea
                      name="message"
                      id="message"
                      className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    ></textarea>
                  </div>
                </div>
                <div className="flex gap-x-4 sm:col-span-2"></div>
              </div>
              <div className="mt-2">
                <ReCAPTCHA
                  sitekey={"11123123213"}
                  onChange={(value) => setRecaptchaValue(value)}
                />
              </div>
              <div className="mt-2">
                <button
                  type="submit"
                  className="px-6 py-1 rounded-xl bg-green-400 cursor-pointer text-white transition-all ease-out"
                  disabled={loading}
                >
                  {loading == false ? "Let's Talk" : "Loading"}
                </button>
              </div>
            </form>
          </div>
          <div className="text-gray-700 flex flex-col">
            <div className="">
              <h2 className="text-2xl font-semibold mb-4">Info Tambahan</h2>
              <div className="grid grid-cols-1 md:grid-cols-2">
                <div className="flex flex-col">
                  <div>
                    <p>
                      <strong>Alamat:</strong> Jl. Raya Pamarayan-Rangkas Bitung
                      Kp. Leuwi Banteng Km.05 Ds. Sangiang Kec. Pamarayan
                      Kabupaten Serang-Banten
                    </p>
                    <p>
                      <strong>No Telp:</strong> 021 222-222-22
                    </p>
                    <p>
                      <strong>Email:</strong> min4serang09@gmail.com
                    </p>
                  </div>
                  <div className="mt-3">
                    <iframe
                      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.753622427344!2d106.82327517589971!3d-6.296073761623477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3b69208f7bd%3A0x13f7eb9520cd483e!2sAD%20Premier!5e0!3m2!1sid!2sid!4v1705814261229!5m2!1sid!2sid"
                      width="300"
                      height="300"
                    ></iframe>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </Wrapper>
  );
};

export default Index;
