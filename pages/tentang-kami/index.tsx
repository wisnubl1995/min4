import React, { useEffect } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import Breadcrumb from "@/components/Breadcrumb";
import Footer from "@/components/Footer";
import Header from "@/components/Header";
import Wrapper from "@/components/Wrapper";
import { api } from "@/lib/api";
import { KAMI } from "@/lib/query";
import * as d3 from "d3";

type Props = {};

const drawChart = () => {
  const data: any = {
    name: "John Doe (CEO)",
    position: "CEO",
    children: [
      { name: "Jane Smith (COO)", position: "COO" },
      { name: "Mark Johnson (Manager)", position: "Manager" },
      // Tambahkan lebih banyak anak sesuai kebutuhan
    ],
  };

  const width = 600;
  const height = 400;

  const svg = d3
    .select(".chart-container")
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(50, 50)");

  const tree = d3.tree().size([width - 100, height - 100]);

  const root = d3.hierarchy(data);
  tree(root);

  const link: any = svg
    .selectAll(".link")
    .data(root.links())
    .enter()
    .append("path")
    .attr("class", "link")
    .attr("d");

  const node = svg
    .selectAll(".node")
    .data(root.descendants())
    .enter()
    .append("g")
    .attr("class", "node")
    .attr("transform", (d: any) => `translate(${d.y},${d.x})`);

  node.append("circle").attr("r", 10);

  node
    .append("text")
    .attr("dy", "0.31em")
    .attr("x", (d: any) => (d.children ? -13 : 13))
    .style("text-anchor", (d: any) => (d.children ? "end" : "start"))
    .text((d: any) => `${d.data.name} (${d.data.position})`);
};

export const getServerSideProps = async () => {
  const { tentangKami }: any = await api.request(KAMI);

  return {
    props: {
      tentangKami,
    },
  };
};

const About = ({ tentangKami }: any) => {
  return (
    <Wrapper>
      <Header />
      <Breadcrumb title={"Tentang Kami"} />

      <div className="w-full py-3 lg:py-10 lg:px-0 px-2">
        <Tabs>
          <div className="cursor-pointer">
            <TabList
              style={{
                display: "flex",
                justifyContent: "flex-start",
                marginBottom: "20px",
              }}
            >
              <Tab
                style={{
                  backgroundColor: "#4CAF50",
                  color: "#fff",
                  padding: "10px 20px",
                  borderRadius: "5px",
                  marginRight: "10px",
                }}
              >
                Profil
              </Tab>
              <Tab
                style={{
                  backgroundColor: "#2196F3",
                  color: "#fff",
                  padding: "10px 20px",
                  borderRadius: "5px",
                  marginRight: "10px",
                }}
              >
                Sejarah
              </Tab>
              <Tab
                style={{
                  backgroundColor: "#FFC107",
                  color: "#fff",
                  padding: "10px 20px",
                  borderRadius: "5px",
                  marginRight: "10px",
                }}
              >
                Struktur
              </Tab>
              <Tab
                style={{
                  backgroundColor: "#FF5722",
                  color: "#fff",
                  padding: "10px 20px",
                  borderRadius: "5px",
                  marginRight: "10px",
                }}
              >
                Sebaran Siswa
              </Tab>
            </TabList>
          </div>

          <div>
            <TabPanel>
              <div
                style={{
                  backgroundColor: "#4CAF50",
                  color: "#fff",
                  padding: "20px",
                  borderRadius: "5px",
                }}
              >
                <div className="flex flex-col gap-5 px-3">
                  <div>
                    <h3 className="text-xl font-bold">VISI</h3>
                    <blockquote className="italic">
                      "{tentangKami.visi}"
                    </blockquote>
                  </div>
                  <div>
                    <h3 className="text-xl font-bold">MISI</h3>
                    <span
                      className="html"
                      dangerouslySetInnerHTML={{
                        __html: tentangKami.misi.html,
                      }}
                    ></span>
                  </div>
                </div>
              </div>
            </TabPanel>
            <TabPanel>
              <div
                style={{
                  backgroundColor: "#2196F3",
                  color: "#fff",
                  padding: "20px",
                  borderRadius: "5px",
                }}
              >
                Sejarah Content
              </div>
            </TabPanel>
            <TabPanel>
              <div
                style={{
                  backgroundColor: "#FFC107",
                  color: "#fff",
                  padding: "20px",
                  borderRadius: "5px",
                }}
              >
                <table></table>
              </div>
            </TabPanel>
            <TabPanel>
              <div
                style={{
                  backgroundColor: "#FF5722",
                  color: "#fff",
                  padding: "20px",
                  borderRadius: "5px",
                }}
              >
                Sebaran Siswa Content
              </div>
            </TabPanel>
          </div>
        </Tabs>
      </div>

      <Footer />
    </Wrapper>
  );
};

export default About;
